use std::{
    f32::consts::{PI, TAU},
    time::Duration,
};

use bevy::{
    gltf::{Gltf, GltfMesh},
    prelude::*,
    render::mesh::{Indices, VertexAttributeValues},
};
use bevy_rapier3d::prelude::*;
use smooth_bevy_cameras::{
    controllers::fps::{FpsCameraBundle, FpsCameraController, FpsCameraPlugin},
    LookTransform, LookTransformBundle, LookTransformPlugin, Smoother,
};

fn main() {
    App::new()
        .insert_resource(ClearColor(Color::rgb(
            0xF9 as f32 / 255.0,
            0xF9 as f32 / 255.0,
            0xFF as f32 / 255.0,
        )))
        .insert_resource(AmbientLight {
            color: Color::WHITE,
            brightness: 1.0 / 5.0f32,
        })
        .insert_resource(Msaa::default())
        .insert_resource(AssetsLoaded(false))
        .add_event::<GltfLoaded>()
        .add_plugins(DefaultPlugins)
        .add_plugin(LookTransformPlugin)
        .add_plugin(FpsCameraPlugin::default())
        .add_plugin(RapierPhysicsPlugin::<NoUserData>::default())
        // .add_plugin(RapierDebugRenderPlugin::default())
        .add_startup_system(setup)
        // .add_startup_system(setup_physics)
        .add_startup_system(load_gltf)
        .add_system(spawn_gltf_objects)
        // .insert_resource(BallState::default())
        .add_system(spawn_balls)
        // .add_system(ball_spawner)
        // .add_system(move_ball)
        .run();
}

#[derive(Component)]
pub struct Player;

pub fn setup(
    mut command: Commands,
    mut meshes: ResMut<Assets<Mesh>>,
    mut materials: ResMut<Assets<StandardMaterial>>,
) {
    command.spawn_bundle(FpsCameraBundle::new(
        FpsCameraController {
            smoothing_weight: 0.1,
            ..default()
        },
        PerspectiveCameraBundle::default(),
        Vec3::new(0., 80., 0.),
        Vec3::ZERO,
    ));

    // ball spawner
    command.spawn().insert(Spawner {
        timer: Timer::new(Duration::from_secs_f32(0.5), true),
    });

    // command.spawn_bundle(DirectionalLightBundle {
    //     directional_light: DirectionalLight {
    //         // Configure the projection to better fit the scene
    //         shadow_projection: OrthographicProjection {
    //             far: 1000.0,
    //             ..default()
    //         },
    //         shadows_enabled: true,
    //         ..default()
    //     },
    //     transform: Transform {
    //         translation: Vec3::new(0., 3000., 1000.),
    //         rotation: Quat::from_rotation_x(-PI / 6.),
    //         ..default()
    //     },
    //     ..default()
    // });

    command
        .spawn_bundle(PointLightBundle {
            transform: Transform::from_translation(Vec3::Y * 10.),
            point_light: PointLight {
                intensity: 32000.,
                color: Color::WHITE,
                shadows_enabled: true,
                range: 500.,
                radius: 0.5,
                ..default()
            },
            ..default()
        })
        .with_children(|builder| {
            builder.spawn_bundle(PbrBundle {
                mesh: meshes.add(Mesh::from(shape::UVSphere {
                    radius: 0.5,
                    ..default()
                })),
                material: materials.add(StandardMaterial {
                    base_color: Color::ORANGE_RED,
                    ..default()
                }),
                ..default()
            });
        });

    command
        .spawn_bundle(PointLightBundle {
            transform: Transform::from_translation(Vec3::Y * 50.),
            point_light: PointLight {
                intensity: 32000.,
                color: Color::GOLD,
                shadows_enabled: true,
                range: 500.,
                radius: 1.,
                ..default()
            },
            ..default()
        })
        .with_children(|builder| {
            builder.spawn_bundle(PbrBundle {
                mesh: meshes.add(Mesh::from(shape::UVSphere {
                    radius: 0.5,
                    ..default()
                })),
                material: materials.add(StandardMaterial {
                    base_color: Color::GOLD,
                    ..default()
                }),
                ..default()
            });
        });
    command
        .spawn_bundle(PointLightBundle {
            transform: Transform::from_translation(Vec3::Y * 80.),
            point_light: PointLight {
                intensity: 32000.,
                color: Color::RED,
                shadows_enabled: true,
                range: 500.,
                radius: 0.5,
                ..default()
            },
            ..default()
        })
        .with_children(|builder| {
            builder.spawn_bundle(PbrBundle {
                mesh: meshes.add(Mesh::from(shape::UVSphere {
                    radius: 0.5,
                    ..default()
                })),
                material: materials.add(StandardMaterial {
                    base_color: Color::ORANGE_RED,
                    ..default()
                }),
                ..default()
            });
        });
}

#[derive(Component)]
pub struct Spawner {
    timer: Timer,
}

pub fn spawn_balls(
    mut command: Commands,
    mut meshes: ResMut<Assets<Mesh>>,
    mut materials: ResMut<Assets<StandardMaterial>>,
    time: Res<Time>,
    mut query: Query<&mut Spawner>,
) {
    let mut spawner = query.get_single_mut().unwrap();
    spawner.timer.tick(time.delta());

    if spawner.timer.just_finished() {
        command
            .spawn_bundle(PbrBundle {
                mesh: meshes.add(Mesh::from(shape::UVSphere {
                    radius: 1.,
                    ..Default::default()
                })),
                material: materials.add(Color::ORANGE.into()),
                transform: Transform::from_translation(Vec3::Y * 70.),
                ..default()
            })
            .insert(RigidBody::Dynamic)
            .insert(Velocity::linear(Vec3::Z * 20.))
            .insert(Damping {
                linear_damping: 0.1,
                angular_damping: 0.5,
            })
            .insert(Friction::coefficient(0.7))
            .insert(Restitution::coefficient(0.9))
            .insert(Collider::ball(1.));
    }
}

fn move_ball(
    keys: Res<Input<KeyCode>>,
    mut query: Query<(&mut Velocity, &Transform), With<Player>>,
) {
    let v = 100.;
    for (mut vel, transform) in query.iter_mut() {
        if keys.pressed(KeyCode::Up) {
            vel.linvel = transform.rotation * Vec3::Z * v;
        }

        if keys.just_released(KeyCode::Up) {
            vel.linvel = transform.rotation * Vec3::Z * 0.;
        }

        if keys.pressed(KeyCode::Down) {
            vel.linvel = transform.rotation * -Vec3::Z * v;
        }

        if keys.just_released(KeyCode::Down) {
            vel.linvel = transform.rotation * Vec3::Z * 0.;
        }

        if keys.pressed(KeyCode::Right) {
            vel.angvel += transform.rotation * Vec3::X * v;
        }

        if keys.just_released(KeyCode::Right) {
            vel.angvel += transform.rotation * Vec3::X * 0.;
        }

        if keys.pressed(KeyCode::Left) {
            vel.angvel += transform.rotation * -Vec3::X * v;
        }

        if keys.just_released(KeyCode::Left) {
            vel.angvel += transform.rotation * Vec3::X * 0.;
        }
    }
}

struct MyAssetPack(Handle<Gltf>);

fn load_gltf(mut commands: Commands, ass: Res<AssetServer>) {
    // let handle: Handle<Gltf> = ass.load("tokyo/scene.gltf");
    let handle: Handle<Gltf> = ass.load("terrain.glb");
    commands.insert_resource(MyAssetPack(handle));
}
struct GltfLoaded(Handle<Gltf>);
struct AssetsLoaded(bool);

fn spawn_gltf_objects(
    mut cmd: Commands,
    my: Res<MyAssetPack>,
    assets_gltf: Res<Assets<Gltf>>,
    // server: Res<AssetServer>,
    assets_gltf_mesh: Res<Assets<GltfMesh>>,
    assets_mesh: Res<Assets<Mesh>>,
    mut asset_loaded: ResMut<AssetsLoaded>,
) {
    if asset_loaded.0 {
        return;
    };
    println!("span gltf objects gets called");

    if let Some(gltf) = assets_gltf.get(&my.0) {
        // spawn scene
        let scene0 = gltf.scenes[0].clone();
        cmd.spawn_scene(scene0);

        for handle in &gltf.meshes {
            let cube_mesh = assets_gltf_mesh.get(handle).unwrap();

            let mesh_handle: Handle<Mesh> = cube_mesh.primitives[0].mesh.clone();
            let mesh = assets_mesh.get(mesh_handle).unwrap();

            let vertices = mesh.attribute(Mesh::ATTRIBUTE_POSITION).unwrap();
            let indices = mesh.indices().unwrap();
            let vertices = match vertices {
                VertexAttributeValues::Float32x3(val) => val,
                _ => {
                    println!("not [f32; 3]!");
                    return;
                }
            }
            .iter()
            .map(|v| Vec3::from_slice(v))
            .collect::<Vec<Vec3>>();

            println!("vertices length: {}", vertices.len());

            let indices = match indices {
                Indices::U32(val) => val,
                _ => {
                    println!("not U32!");
                    return;
                }
            }
            .chunks(3)
            .map(|v| [v[0], v[1], v[2]])
            .collect();

            cmd.spawn()
                .insert(RigidBody::Fixed)
                .insert(Terrain)
                .insert(Transform::from_translation(Vec3::ONE * 3.))
                .with_children(|c| {
                    c.spawn().insert(Collider::trimesh(vertices, indices));
                });
        }

        asset_loaded.0 = true;
    }
}

#[derive(Component)]
pub struct Terrain;
